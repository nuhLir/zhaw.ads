/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.todolist;

import ch.zhaw.ads.utils.MyQueue;

/**
 *
 * @author iliraga
 */
public class TodoList {
    private final MyQueue<String> queue = new MyQueue<>();
    
    /**
     * Adds the task into the todolist
     * @param object 
     */
    public void add(String object){
        queue.add(object);
    }

    /**
     * Removes the first entry of the todolist and retrieves it
     * @return 
     */
    public String remove(){
        return queue.dequeue();
    }
}
