/**
 * AnyServer -- Praktikum Experimentierkasten -- ADS
 *
 * @author E. Mumprecht
 * @version 1.0 -- Geruest fuer irgendeinen Server
 */
package ch.zhaw.ads;

public class P01A2_kgV implements CommandExecutor {

    /**
     * Executes this particular command
     *
     * @param command
     * @return
     */
    @Override
    public String execute(String command) {
        String[] parameters = command.split("\\,");

        int first = Integer.parseInt(parameters[0]);
        int second = Integer.parseInt(parameters[1]);

        return Integer.toString(kgV(first, second));

    }

    /**
     * Retrieves the kgV of both numbers
     * @param first
     * @param second
     * @return 
     */
    private int kgV(int first, int second) {
        return first * second / ggT(first, second);
    }
    
    /**
     * Retrieves the ggT
     * 
     * @param first
     * @param second
     * @return 
     */
    private int ggT(int first, int second) {
        while (second != 0) {
            if (first > second) {
                first = first - second;
            } else {
                second = second - first;
            }
        }
        
        return first;
    }
}
