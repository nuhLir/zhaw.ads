/**
 * AnyServer -- Praktikum Experimentierkasten -- ADS
 *
 * @author E. Mumprecht
 * @version 1.0 -- Geruest fuer irgendeinen Server
 */
package ch.zhaw.ads;

import ch.zhaw.ads.utils.MyStack;

public class P01A3_stack implements CommandExecutor {
    private MyStack<String> stack = null;
    
    /**
     * Executes this particular command
     *
     * @param command
     * @return
     */
    @Override
    public String execute(String command) {
        if (stack == null) {
            stack = new MyStack();
        }
        
        String[] parameters = command.split("\\ ");
        
        String commandName = parameters[0].trim();
        String commandArgument = "";
        
        if (parameters.length > 1){
            commandArgument = parameters[1];
        }
        
        switch(commandName.toLowerCase()) {
            case "push":
                stack.push(commandArgument);
                return "";
            case "pop":
                return stack.pop();
            case "peek":
                return stack.peek(Integer.parseInt(commandArgument));
            case "isempty":
                return Boolean.toString(stack.isEmpty());
            case "isfull":
                return Boolean.toString(stack.isFull());
        }

        return "command not found '"+commandName+"'";
    }
}
