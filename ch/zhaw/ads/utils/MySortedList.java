/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.utils;

/**
 *
 * @author iliraga
 * @param <T>
 */
public class MySortedList<T extends Comparable> extends MyList<T> {
    public boolean insert(T node){
        if (size() == 0) {
            super.add(node);
            return true;
        }
        
        MyListNode<T> current = getFirst();
        MyListNode<T> last = null;
        int lastIndex = -1;
        
        while(current != null && node.compareTo(current.getContent()) < 0) {
            last = current;
            current = current.getNext();
            lastIndex++;
        }
        
        if (last != null){
            add(lastIndex, node);
        }
        
        return true;
    }
}
