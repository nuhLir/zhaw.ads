/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.utils;

/**
 *
 * @author iliraga
 */
public class MyListNode<T> {
    private T content = null;
    private MyListNode<T> next = null;
    
    /**
     * Constructor
     * @param content 
     */
    public MyListNode(T content){
        this.content = content;
    }
    
    /**
     * Sets the next element
     * @param next 
     */
    public void setNext(MyListNode<T> next){
        this.next = next;
    }
    
    /**
     * Gets the content of this node
     * @return 
     */
    public T getContent(){
        return content;
    }
    
    /**
     * Gets the next node
     * @return 
     */
    public MyListNode<T> getNext(){
        return next;
    }
}
