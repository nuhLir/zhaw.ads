/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author iliraga
 * @param <T>
 */
public class MyQueue<T> {
    private final List<T> list = new ArrayList<>();
    
    /**
     * Adds the specific object into the list
     * 
     * @param object 
     */
    public void add(T object){
        list.add(object);
    }
    
    /**
     * Gets the first item from the queue
     * @return 
     */
    public T dequeue(){
        T result = list.get(0);
        list.remove(0);
        
        return result;
    }
    
    /**
     * Gets all entries of the whole queue
     * @return 
     */
    public List<T> getList(){
        return list;
    }
    
    /**
     * Checks whether the queue is empty rightnow
     * @return 
     */
    public boolean isEmpty(){
        return list.isEmpty();
    }
}
