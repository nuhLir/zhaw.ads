/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.utils;

import java.util.AbstractList;

/**
 *
 * @author iliraga
 * @param <T>
 */
public class MyList<T> extends AbstractList<T>{
    private MyListNode<T> first = null;
    private MyListNode<T> last = null;
    private int lastNodeIndex = 0;

    @Override
    public boolean add(T node){
        if (isEmpty()){
            first = last = new MyListNode<>(node);
        } else {
            // go to last item and make sure we link the next entry to it
            // aka append it
            MyListNode<T> next = new MyListNode<>(node);
            
            last.setNext(next);
            last = next;
        }
        
        lastNodeIndex++;
        
        return true;
    }

    /**
     * Adds at the specific index and shifts up everything from that point
     * @param index
     * @param node 
     */
    @Override
    public void add(int index, T node) {
        System.out.println(this+" " + Integer.toString(index));
        
        if (isEmpty() || index > size() - 1){
            add(node);
        } else {
            System.out.println(index);
            
            MyListNode<T> last = getNode(index);
            MyListNode<T> next = last.getNext();
            MyListNode<T> inserted = new MyListNode<>(node);
            
            inserted.setNext(next);
            last.setNext(inserted);
        }
        
        System.out.println(this);
    }
    
    @Override
    public T get(int index) {
        return getNode(index).getContent();
    }

    @Override
    public T remove(int index){
        MyListNode<T> previous = first;
        MyListNode<T> deletedNode = first;
        
        if (index > 0){
            previous = getNode(index - 1);
            deletedNode = previous.getNext();
        }
        
        MyListNode<T> next = deletedNode.getNext();
        
        if (index == 0){
            first = next;
        }
        
        // link the previous item with the next
        // to overbridge the deleted node
        previous.setNext(next);
        
        lastNodeIndex--;
        
        // retrieve the content of it
        // and let the GC clean out this shizzl
        return deletedNode.getContent();
    }
    
    @Override
    public int size() {
        return lastNodeIndex;
    }
    
    @Override
    public boolean isEmpty(){
        return size() == 0;
    }
    
    @Override
    public String toString(){
        MyListNode<T> current = getFirst();
        String result = "";
        
        while(current != null){
            result += current.getContent().toString() + " ";
            current = current.getNext();
        }
        
        return result;
    }
    
    /**
     * Retrieves the first element
     * @return 
     */
    protected MyListNode<T> getFirst(){
        return first;
    }
    
    /**
     * Gets the node at the specific index
     * 
     * @param index
     * @return 
     */
    protected MyListNode<T> getNode(int index){
        if (index > size() - 1){
            throw new IndexOutOfBoundsException("Exceeded list, has only "+size()+" items");
        }
        
        MyListNode<T> current = first;
        
        for (int i = 0; i < index; i++){
            current = current.getNext();
            
            if (current == null){
                throw new IndexOutOfBoundsException();
            }
        }
        
        return current;
    }
}
