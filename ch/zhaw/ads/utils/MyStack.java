/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads.utils;

import java.util.ArrayList;

/**
 *
 * @author iliraga
 * @param <T> The type which should be stacked
 */
public class MyStack<T> {
    private final ArrayList<T> list = new ArrayList<>();
    
    /**
     * Push new element onto stack
     * @param element 
     */
    public void push(T element){
        list.add(0, element);
    }
    
    /**
     * Gets the current stacked element
     * @return 
     */
    public T pop(){
        if (isEmpty()) {
            // tries to pop, but no more element there
            // throw sumpn
            throw new IndexOutOfBoundsException("Stack is empty");
        }
        
        T element = list.get(0);
        list.remove(0);
        
        return element;
    }
    
    /**
     * Peeks the position specified
     * @param position
     * @return 
     */
    public T peek(int position){
        return list.get(position);
    }
    
    /**
     * Peeks the top position without popping it
     * @return 
     */
    public T peek(){
        return list.get(0);
    }
    
    /**
     * Checks whether the list is empty
     * @return 
     */
    public boolean isEmpty(){
        return list.isEmpty();
    }
    
    /**
     * Never ending stack here
     * @return 
     */
    public boolean isFull(){
        return false;
    }
    
    public String ping(){
        return "ping from stack";
    }
}
