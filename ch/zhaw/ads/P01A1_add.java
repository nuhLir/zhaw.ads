/**
 * AnyServer -- Praktikum Experimentierkasten -- ADS
 *
 * @author E. Mumprecht
 * @version 1.0 -- Geruest fuer irgendeinen Server
 */
package ch.zhaw.ads;

public class P01A1_add implements CommandExecutor {
    /**
     * Executes this particular command
     * @param command
     * @return 
     */
    @Override
    public String execute(String command) {
        String[] parameters = command.split("\\+");

        int first = Integer.parseInt(parameters[0]);
        int second = Integer.parseInt(parameters[1]);

        return Integer.toString(first + second);
    }
}

