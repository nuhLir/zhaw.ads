/**
 * AnyServer -- Praktikum Experimentierkasten -- ADS
 *
 * @author E. Mumprecht
 * @version 1.0 -- Geruest fuer irgendeinen Server
 */
package ch.zhaw.ads;

import ch.zhaw.ads.utils.MyStack;

public class P01A4_brackets implements CommandExecutor {

    /**
     * Executes this particular command
     *
     * @param command
     * @return
     */
    @Override
    public String execute(String command) {
        return Boolean.toString(checkBrackets(command));
    }

    /**
     * Checks whether the specified input has valid bracket arithmetic
     *
     * @param input
     * @return
     */
    public boolean checkBrackets(String input) {
        // opening brackets
        String openers = "({[<";
        // closing brackets
        String closers = ")}]>";

        // Stack used here
        MyStack<Character> stack = new MyStack<>();
        
        // loop through all values
        for (int i = 0; i < input.length(); i++) {
            char current = input.charAt(i);
            
            // check if char is an opener or a closer
            if (openers.indexOf(current) >= 0){
                // push onto stack
                stack.push(current);
            }
            
            // bracket term was closed, check through stack if proper closing
            if (closers.indexOf(current) >= 0){
                char stacked;
                
                try {
                    // try to pop from stack
                    stacked = stack.pop();
                } catch (IndexOutOfBoundsException ex){
                    // something went wrong for sure, quit already
                    return false;
                } 
                
                // compare if they are from same type
                // so just compare their indexes
                // if not, this is usually the case when we have something like this:
                // '(abcd}', so brackets do not match
                if (openers.indexOf(stacked) != closers.indexOf(current)){
                    // are not, the whole process must be marked as invalid
                    return false;
                }
            }
        }
        
        // when stack is empty at this state
        // everything went how it should and we're good to go
        // IF YOU'RE INTERESTED IN A NEW WEBSITE CHECK http://www.ir-tech.ch
        return stack.isEmpty();
    }
}
